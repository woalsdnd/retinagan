import argparse
import os
import numpy as np
from scipy.stats import ks_2samp
import itertools


def get_best_results(filename):
    lines = [line.rstrip('\n') for line in open(filename)]
    line_count = 0
    turn = 0
    best_gan_loss = 10000  # assume it's max
    for line in lines:
#         if line_count == 10:
#             break
        
        if "Round" in line:
            turn += 1
            turn = turn % 3
            if turn == 2:  # gan line
                s = line.index("loss")
                gan_loss = float(line[s + line[s:].index(":") + 1:s + line[s:].index(",")])
                if gan_loss < best_gan_loss:
                    best_gan_loss = gan_loss
                    renew = True
            if turn == 0:  # testing line
                line_count += 1
                if renew:
                    s = line.index("auc_pr")
                    auc_pr = float(line[s + line[s:].index(":") + 1:s + line[s:].index(",")])
                    s = line.index("auc_roc")
                    auc_roc = float(line[s + line[s:].index(":") + 1:s + line[s:].index(",")])
                    renew = False
    
    return auc_pr, auc_roc


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--dir_outfiles',
    type=str,
    help="Directory of out files",
    required=True
    )
FLAGS, _ = parser.parse_known_args()

tasks = ["optic_disc"]
models = ["unet", "pixel", "patch1", "patch2", "image"]
curves = ["pr", "roc"]

dicts = dict(zip(zip(tasks * 10, models * 2, curves * 5), [[] for _ in range(10)]))

outfiles = all_files_under(FLAGS.dir_outfiles)

for outfile in outfiles:
    for task in tasks:
        for model in models: 
            if task in outfile and model in outfile:
                pr, roc = get_best_results(outfile)
                dicts[(task, model, "pr")].append(pr)
                dicts[(task, model, "roc")].append(roc)

for group, results in sorted(dicts.items(), key=lambda x:(x[0][2], x[0][1])):
    print "{} --- mean : {}, std : {}".format(group, np.mean(results), np.std(results, ddof=1))

for pair in itertools.combinations(["unet", "image", "patch1", "patch2", "pixel"], 2):
    print "{} vs {}".format(pair[0], pair[1])
    print ks_2samp(dicts[('optic_disc', pair[0], 'pr')], dicts[('optic_disc', pair[1], 'pr')])
    print ks_2samp(dicts[('optic_disc', pair[0], 'roc')], dicts[('optic_disc', pair[1], 'roc')])

