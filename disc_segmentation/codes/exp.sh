for type in unet; do for i in 1 2 3 4 5; do python train.py --ratio_gan2seg=0 --gpu_index=0 --discriminator=${type} --batch_size=4 --dataset=drishti > ../results/drishti/out_optic_disc_${type}_${i};  done ; done

for type in image; do for i in 1 2 3 4 5; do python train.py --ratio_gan2seg=10 --gpu_index=0 --discriminator=${type} --batch_size=4 --dataset=drishti > ../results/drishti/out_optic_disc_${type}_${i};  done ; done

for type in patch1; do for i in 1 2 3 4 5; do python train.py --ratio_gan2seg=10 --gpu_index=0 --discriminator=${type} --batch_size=4 --dataset=drishti > ../results/drishti/out_optic_disc_${type}_${i};  done ; done

for type in patch2; do for i in 1 2 3 4 5; do python train.py --ratio_gan2seg=10 --gpu_index=0 --discriminator=${type} --batch_size=4 --dataset=drishti > ../results/drishti/out_optic_disc_${type}_${i};  done ; done

for type in pixel; do for i in 1 2 3 4 5; do python train.py --ratio_gan2seg=10 --gpu_index=0 --discriminator=${type} --batch_size=2 --dataset=drishti >  ../results/drishti/out_optic_disc_${type}_${i};  done ; done