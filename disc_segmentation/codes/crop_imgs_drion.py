import utils
from PIL import Image
import numpy as np

files=utils.all_files_under("../pixel_gan_drion")
ori_shape=(1,400,600)

for file in files:
    print "processing {}...".format(file)
    img=utils.imagefiles2arrs([file])
    cropped=utils.crop_to_original(img, ori_shape)
    Image.fromarray(cropped[0,...].astype(np.uint8)).save(file)
