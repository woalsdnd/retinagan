import utils
import os
from skimage import measure
import numpy as np
from sklearn.metrics import roc_curve, precision_recall_curve
from sklearn.metrics.classification import confusion_matrix
from PIL import Image
from scipy.stats import ks_2samp

# set output directories
home_dirs = ["../data/preprocessed/drion/", "../data/preprocessed/rim_one", "../data/preprocessed/drishti"]
for home_dir in home_dirs:
    print "="*30
    print home_dir
    print "="*30
    
    dir_gt = os.path.join(home_dir, "test/masks")
    dir_unet = os.path.join(home_dir, "test/unet")
    dir_pixelgan = os.path.join(home_dir, "test/pixelgan")
    if "drishti" not in home_dir:
        dir_driu = os.path.join(home_dir, "test/DRIU")
        dir_human = os.path.join(home_dir, "test/2nd_manual")
    dir_fundus = os.path.join(home_dir, "test/images")
    dir_contour = os.path.join(home_dir, "contour")
    dir_curve = os.path.join(home_dir, "curve")
    
    if not os.path.isdir(dir_curve):
        os.makedirs(dir_curve)
    if not os.path.isdir(dir_contour):
        os.makedirs(dir_contour)
        
    # load images 
    gt_od = utils.load_images_under_dir(dir_gt) / 255
    gt_od[gt_od < 0.9] = 0    
    gt_od[gt_od >= 0.9] = 1    
    out_unet = utils.load_images_under_dir(dir_unet) / 255
    out_pixelgan = utils.load_images_under_dir(dir_pixelgan) / 255
    if "drishti" not in home_dir:
        human_expert_od = utils.load_images_under_dir(dir_human) / 255
        human_expert_od[human_expert_od < 0.9] = 0
        human_expert_od[human_expert_od >= 0.9] = 1
        driu_od = utils.load_images_under_dir(dir_driu) / 255
    fundus = utils.load_images_under_dir(dir_fundus)
    
    # generate masks, get pixels in masks 
    masks = utils.mask(utils.load_images_under_dir(dir_fundus), 20)
    gt_od_in_mask, out_unet_in_mask = utils.pixel_values_in_mask(gt_od, out_unet , masks)
    gt_od_in_mask, out_pixelgan_in_mask = utils.pixel_values_in_mask(gt_od, out_pixelgan , masks)
    if "drishti" not in home_dir:
        driu_in_mask, human_expert_od_in_mask = utils.pixel_values_in_mask(driu_od, human_expert_od , masks)
                 
    # draw contour for each image
    distance_unet, distance_driu, distance_human_expert, distance_pixelgan = [], [], [], []
    for index in range(gt_od.shape[0]):
        thresholded_unet = utils.threshold_by_f1(np.expand_dims(gt_od[index, ...], axis=0),
                                                      np.expand_dims(out_unet[index, ...], axis=0),
                                                      np.expand_dims(masks[index, ...], axis=0),
                                                      flatten=False)
        thresholded_pixelgan = utils.threshold_by_f1(np.expand_dims(gt_od[index, ...], axis=0),
                                                      np.expand_dims(out_pixelgan[index, ...], axis=0),
                                                      np.expand_dims(masks[index, ...], axis=0),
                                                      flatten=False)
        if "drishti" not in home_dir:
            thresholded_driu = utils.threshold_by_f1(np.expand_dims(gt_od[index, ...], axis=0),
                                                          np.expand_dims(driu_od[index, ...], axis=0),
                                                          np.expand_dims(masks[index, ...], axis=0),
                                                          flatten=False)
        contours_unet = measure.find_contours(np.squeeze(thresholded_unet, axis=0), 0.9)
        contours_pixelgan = measure.find_contours(np.squeeze(thresholded_pixelgan, axis=0), 0.9)
        contours_gt = measure.find_contours(gt_od[index, ...], 0.9)
        distance_unet.append(utils.min_distance(contours_unet[0], contours_gt[0]))
        distance_pixelgan.append(utils.min_distance(contours_pixelgan[0], contours_gt[0]))
        if "drishti" not in home_dir:
            contours_driu = measure.find_contours(np.squeeze(thresholded_driu, axis=0), 0.9)
            contours_human_expert = measure.find_contours(human_expert_od[index, ...], 0.9)
            distance_driu.append(utils.min_distance(contours_driu[0], contours_gt[0]))
            distance_human_expert.append(utils.min_distance(contours_human_expert[0], contours_gt[0]))
        
        if "drishti" not in home_dir:
            for i in range(len(contours_unet[0])):
                fundus[index, int(contours_unet[0][i][0]), int(contours_unet[0][i][1]), ...] = (0, 0, 255)
            for i in range(len(contours_gt[0])):
                fundus[index, int(contours_gt[0][i][0]), int(contours_gt[0][i][1]), ...] = (0, 255, 0)
            for i in range(len(contours_human_expert[0])):
                fundus[index, int(contours_human_expert[0][i][0]), int(contours_human_expert[0][i][1]), ...] = (255, 255, 0)
            Image.fromarray(fundus[index, ...].astype(np.uint8)).save(os.path.join(dir_contour, "{}.png".format(index)))
    
    print "unet -- mean : {}, std : {}".format(np.mean(distance_unet), np.std(distance_unet))
    print "pixelgan -- mean : {}, std : {}".format(np.mean(distance_pixelgan), np.std(distance_pixelgan))
    if "drishti" not in home_dir:
        print "driu -- mean : {}, std : {}".format(np.mean(distance_driu), np.std(distance_driu))
        print "2nd Annotator -- mean : {}, std : {}".format(np.mean(distance_human_expert), np.std(distance_human_expert))
        print ks_2samp(distance_unet, distance_human_expert)
    
    # draw ROC, PR curve and compute Dice Coeff
    print "UNET"
    print "dice coefficient : {}".format(utils.dice_coefficient(gt_od, out_unet, masks))
    
    print "pixelgan"
    print "dice coefficient : {}".format(utils.dice_coefficient(gt_od, out_pixelgan, masks))
    
    if "drishti" not in home_dir:
        print "DRIU"
        print "dice coefficient : {}".format(utils.dice_coefficient(gt_od, driu_od, masks))
        
        fpr_our, tpr_our, _ = roc_curve(gt_od_in_mask.flatten(), out_unet_in_mask.flatten())
        prec_our, recall_our, _ = precision_recall_curve(gt_od_in_mask.flatten(), out_unet_in_mask.flatten())

        fpr_driu, tpr_driu, _ = roc_curve(gt_od_in_mask.flatten(), driu_in_mask.flatten())
        prec_driu, recall_driu, _ = precision_recall_curve(gt_od_in_mask.flatten(), driu_in_mask.flatten())

        cm = confusion_matrix(gt_od_in_mask.flatten(), human_expert_od_in_mask.flatten())
        fpr_human = 1 - 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
        tpr_human = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
        prec_human = 1.*cm[1, 1] / (cm[0, 1] + cm[1, 1])
        recall_human = tpr_human
        print "2nd Annotator"
        print "dice coefficient : {}".format(2.*prec_human * recall_human / (prec_human + recall_human))

        human_op_pts_roc, human_op_pts_pr = utils.operating_pts_human_experts(gt_od, human_expert_od, masks)
            
#         utils.plot_AUC_ROC([fpr_our, fpr_driu, fpr_human], [tpr_our, tpr_driu, tpr_human], ["UNET", "DRIU", "2nd_manual"], dir_curve, human_op_pts_roc)
#         utils.plot_AUC_PR([prec_our, prec_driu, prec_human], [recall_our, recall_driu, recall_human], ["UNET", "DRIU", "2nd_manual"], dir_curve, human_op_pts_pr)    
