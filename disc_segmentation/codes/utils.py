import os
import sys

from PIL import Image, ImageEnhance
from keras.preprocessing.image import Iterator
from scipy.ndimage import rotate
from skimage import filters
from sklearn.metrics import auc
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, roc_auc_score, precision_recall_curve
from skimage import measure

import matplotlib.pyplot as plt
import numpy as np
import pickle
import matplotlib
import random


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def imagefiles2arrs(filenames):
    img_shape = image_shape(filenames[0])
    if len(img_shape) == 3:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    elif len(img_shape) == 2:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
    
    for file_index in xrange(len(filenames)):
        img = Image.open(filenames[file_index])
        images_arr[file_index] = np.asarray(img).astype(np.float32)
    
    return images_arr


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def crop_imgs(imgs, pad):
    """
    crop images (4D tensor) by [:,pad:-pad,pad:-pad,:] 
    """
    return imgs[:, pad:-pad, pad:-pad, :]


def make_trainable(net, val):
    net.trainable = val
    for l in net.layers:
        l.trainable = val

        
def discriminator_shape(n, d_out_shape):
    if len(d_out_shape) == 1:  # image gan
        return (n, d_out_shape[0])
    elif len(d_out_shape) == 3:  # pixel, patch gan
        return (n, d_out_shape[0], d_out_shape[1], d_out_shape[2])
    return None


def input2discriminator(real_img_patches, real_mask_patches, fake_mask_patches, d_out_shape):
    real = np.concatenate((real_img_patches, real_mask_patches), axis=3)
    fake = np.concatenate((real_img_patches, fake_mask_patches), axis=3)
    
    d_x_batch = np.concatenate((real, fake), axis=0)
    
    # real : 1, fake : 0
    d_y_batch = np.ones(discriminator_shape(d_x_batch.shape[0], d_out_shape))
    d_y_batch[real.shape[0]:, ...] = 0
    
    return d_x_batch, d_y_batch

 
def input2gan(real_img_patches, real_mask_patches, d_out_shape):    
    g_x_batch = [real_img_patches, real_mask_patches]
    # set 1 to all labels (real : 1, fake : 0)
    g_y_batch = np.ones(discriminator_shape(real_mask_patches.shape[0], d_out_shape))
    return g_x_batch, g_y_batch

    
def print_metrics(itr, **kargs):
    print "*** Round {}  ====> ".format(itr),
    for name, value in kargs.items():
        print ("{} : {}, ".format(name, value)),
    print ""
    sys.stdout.flush()


class TrainBatchFetcher(Iterator):
    """
    fetch batch of original images and mask images
    """

    def __init__(self, train_imgs, train_masks, batch_size):
        self.train_imgs = train_imgs
        self.train_masks = train_masks
        self.n_train_imgs = self.train_imgs.shape[0]
        self.batch_size = batch_size
        
    def next(self):
        indices = list(np.random.choice(self.n_train_imgs, self.batch_size))
        return self.train_imgs[indices, :, :, :], self.train_masks[indices, :, :, :] 


def AUC_ROC(true_mask_arr, pred_mask_arr, save_fname):
    """
    Area under the ROC curve with x axis flipped
    """
    fpr, tpr, _ = roc_curve(true_mask_arr, pred_mask_arr)
#     save_obj({"fpr":fpr, "tpr":tpr}, save_fname)
    AUC_ROC = roc_auc_score(true_mask_arr.flatten(), pred_mask_arr.flatten())
    return AUC_ROC


def operating_pts_human_experts(gt_vessels, pred_vessels, masks):
    gt_vessels_in_mask, pred_vessels_in_mask = pixel_values_in_mask(gt_vessels, pred_vessels , masks, split_by_img=True)

    n = gt_vessels_in_mask.shape[0]
    op_pts_roc, op_pts_pr = [], []
    for i in range(n):
        cm = confusion_matrix(gt_vessels_in_mask[i], pred_vessels_in_mask[i])
        fpr = 1 - 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
        tpr = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
        prec = 1.*cm[1, 1] / (cm[0, 1] + cm[1, 1])
        recall = tpr
        op_pts_roc.append((fpr, tpr))
        op_pts_pr.append((recall, prec))

    return op_pts_roc, op_pts_pr


def plot_AUC_ROC(fprs, tprs, method_names, fig_dir, op_pts):
    # set font style
    font = {'family':'serif'}
    matplotlib.rc('font', **font)
    colors = ['k', 'y', 'r']
    indices = [0, 1, 2] 
    
    # print auc  
    print "****** ROC AUC ******"
    for index in indices:
        if method_names[index] != '2nd_manual':
            print "{} : {:04}".format(method_names[index], auc(fprs[index], tprs[index]))

    # plot results
    for index in indices:
        if method_names[index] == '2nd_manual':
            plt.plot(fprs[index], tprs[index], colors[index] + '*', label='Human')
        else:
#             plt.plot(fprs[index],tprs[index],colors[index], label=method_names[index].replace("_"," "),linewidth=1.5)
            plt.step(fprs[index], tprs[index], colors[index], where='post', label=method_names[index].replace("_", " "), linewidth=1.5)

    # plot individual operation points
    for op_pt in op_pts: 
        plt.plot(op_pt[0], op_pt[1], 'b.')

    plt.title('ROC Curve')
    plt.xlabel("1-Specificity")
    plt.ylabel("Sensitivity")
    plt.xlim(0, 0.03)
    plt.ylim(0.95, 1.0)
    plt.legend(loc="lower right")
    plt.savefig(os.path.join(fig_dir, "ROC.png"))
    plt.close()


def plot_AUC_PR(precisions, recalls, method_names, fig_dir, op_pts):
    # set font style
    font = {'family':'serif'}
    matplotlib.rc('font', **font)
    colors = ['k', 'y', 'r']
    indices = [0, 1, 2] 

    # print auc  
    print "****** Precision Recall AUC ******"
    for index in indices:
        if method_names[index] != '2nd_manual':
            print "{} : {:04}".format(method_names[index], auc(recalls[index], precisions[index]))
    
    # plot results
    for index in indices:
        if method_names[index] == '2nd_manual':
            plt.plot(recalls[index], precisions[index], colors[index] + '*', label='Human')
        else:
#             plt.plot(recalls[index],precisions[index], label=method_names[index].replace("_"," "),linewidth=1.5)
            plt.step(recalls[index], precisions[index], colors[index], where='post', label=method_names[index].replace("_", " "), linewidth=1.5)
    
    # plot individual operation points
    for op_pt in op_pts: 
        plt.plot(op_pt[0], op_pt[1], 'b.')
    
    plt.title('Precision Recall Curve')
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.xlim(0.9, 1.0)
    plt.ylim(0.9, 1.0)
    plt.legend(loc="lower left")
    plt.savefig(os.path.join(fig_dir, "Precision_recall.png"))
    plt.close()

    
def AUC_PR(true_mask_img, pred_mask_img, save_fname):
    """
    Precision-recall curve
    """
    precision, recall, _ = precision_recall_curve(true_mask_img.flatten(), pred_mask_img.flatten(), pos_label=1)
#     save_obj({"precision":precision, "recall":recall}, save_fname)
    AUC_prec_rec = auc(recall, precision)
    return AUC_prec_rec


def save_obj(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open(name, 'rb') as f:
        return pickle.load(f)


def best_f1_threshold(precision, recall, thresholds):
    best_f1 = -1
    for index in range(len(precision)):
        curr_f1 = 2.*precision[index] * recall[index] / (precision[index] + recall[index])
        if best_f1 < curr_f1:
            best_f1 = curr_f1
            best_threshold = thresholds[index]

    return best_f1, best_threshold


def min_distance(A, B):
    dist_list = []
    for el in A:
        dist_list.append(np.min(np.sqrt(np.sum((el - B) ** 2, axis=1))))
    return np.mean(dist_list)


def threshold_by_f1(true_vessels, generated, masks, flatten=True, f1_score=False):
    vessels_in_mask, generated_in_mask = pixel_values_in_mask(true_vessels, generated, masks)
    precision, recall, thresholds = precision_recall_curve(vessels_in_mask.flatten(), generated_in_mask.flatten(), pos_label=1)
    best_f1, best_threshold = best_f1_threshold(precision, recall, thresholds)
    pred_vessels_bin = np.zeros(generated.shape)
    pred_vessels_bin[generated >= best_threshold] = 1
    
    if flatten:
        if f1_score:
            return pred_vessels_bin[masks == 1].flatten(), best_f1
        else:
            return pred_vessels_bin[masks == 1].flatten()
    else:
        if f1_score:
            return pred_vessels_bin, best_f1
        else:
            return pred_vessels_bin
        
    
def misc_measures(true_vessels, pred_vessels, masks):
    thresholded_vessel_arr, f1_score = threshold_by_f1(true_vessels, pred_vessels, masks, f1_score=True)
    true_vessel_arr = true_vessels[masks == 1].flatten()
    
    cm = confusion_matrix(true_vessel_arr, thresholded_vessel_arr)
    acc = 1.*(cm[0, 0] + cm[1, 1]) / np.sum(cm)
    sensitivity = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
    specificity = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
    return f1_score, acc, sensitivity, specificity


def dice_coefficient(true_vessels, pred_vessels, masks):
    thresholded_vessels = threshold_by_f1(true_vessels, pred_vessels, masks, flatten=False)
    
    true_vessels = true_vessels.astype(np.bool)
    thresholded_vessels = thresholded_vessels.astype(np.bool)
    
    intersection = np.count_nonzero(true_vessels & thresholded_vessels)
    
    size1 = np.count_nonzero(true_vessels)
    size2 = np.count_nonzero(thresholded_vessels)
    
    try:
        dc = 2. * intersection / float(size1 + size2)
    except ZeroDivisionError:
        dc = 0.0
    
    return dc


def pad_imgs(imgs, img_size):
    img_h, img_w = imgs.shape[1], imgs.shape[2]
    target_h, target_w = img_size[0], img_size[1]
    if len(imgs.shape) == 4:
        d = imgs.shape[3]
        padded = np.zeros((imgs.shape[0], target_h, target_w, d))
    elif len(imgs.shape) == 3:
        padded = np.zeros((imgs.shape[0], img_size[0], img_size[1]))
    padded[:, (target_h - img_h) // 2:(target_h - img_h) // 2 + img_h, (target_w - img_w) // 2:(target_w - img_w) // 2 + img_w, ...] = imgs
    
    return padded

    
def random_perturbation(imgs):
    for i in range(imgs.shape[0]):
        im = Image.fromarray(imgs[i, ...].astype(np.uint8))
        en = ImageEnhance.Brightness(im)
        im = en.enhance(random.uniform(0.8, 1.2))
        en = ImageEnhance.Color(im)
        im = en.enhance(random.uniform(0.8, 1.2))
        en = ImageEnhance.Contrast(im)
        im = en.enhance(random.uniform(0.8, 1.2))
        imgs[i, ...] = np.asarray(im).astype(np.float32)
    return imgs 


def crop(imgs, img_size):
    if imgs.shape[1] == img_size[0] and imgs.shape[2] == img_size[1]:
        return imgs
    else: 
        return imgs[:, (imgs.shape[1] - img_size[0]) // 2:(imgs.shape[1] - img_size[0]) // 2 + img_size[0],
                    (imgs.shape[1] - img_size[1]) // 2:(imgs.shape[1] - img_size[1]) // 2 + img_size[1], ...]


def mask(imgs, red_threshold):
        
    # Find the Index of Connected Components of the Fundus Area (the central area)
    h_ori, w_ori = imgs.shape[1], imgs.shape[2]
    roi_check_len = 20
    mask_imgs = np.zeros((imgs.shape[0], h_ori, w_ori))
    
    for index in range(imgs.shape[0]):
        blobs_labels = measure.label(imgs[index, :, :, 0] > red_threshold)
        majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                            w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
        mask_imgs[index, ...][np.where(blobs_labels == majority_vote)] = 1

    return mask_imgs

    
def get_imgs(target_dir, augmentation, img_size, fov_mask=False):
    
    img_files = all_files_under(os.path.join(target_dir, "images"))
    mask_files = all_files_under(os.path.join(target_dir, "masks"))
        
    # load images    
    fundus_imgs = imagefiles2arrs(img_files)
    mask_imgs = imagefiles2arrs(mask_files) / 255
    mask_imgs[mask_imgs != 1.0] = 0  # admit only pixels of 255    
    fundus_imgs = pad_imgs(fundus_imgs, img_size)
    mask_imgs = pad_imgs(mask_imgs, img_size)
    n_ori_imgs = fundus_imgs.shape[0]
    assert(np.min(mask_imgs) == 0 and np.max(mask_imgs) == 1)
    if fov_mask:
        assert(fov_mask and not augmentation)
        fov = mask(fundus_imgs, 30)

    # augmentation
    if augmentation:
        # augment the original image (flip, rotate)
        all_fundus_imgs = [fundus_imgs]
        all_mask_imgs = [mask_imgs]
        flipped_imgs = fundus_imgs[:, :, ::-1, :]  # flipped imgs
        flipped_masks = mask_imgs[:, :, ::-1]
        all_fundus_imgs.append(flipped_imgs)
        all_mask_imgs.append(flipped_masks)
        for angle in range(60, 360, 60):  # rotated imgs
            all_fundus_imgs.append(random_perturbation(rotate(fundus_imgs, angle, axes=(1, 2), reshape=False)))
            all_fundus_imgs.append(random_perturbation(rotate(flipped_imgs, angle, axes=(1, 2), reshape=False)))
            all_mask_imgs.append(rotate(mask_imgs, angle, axes=(1, 2), reshape=False))
            all_mask_imgs.append(rotate(flipped_masks, angle, axes=(1, 2), reshape=False))
        fundus_imgs = np.concatenate(all_fundus_imgs, axis=0)
        mask_imgs = np.round((np.concatenate(all_mask_imgs, axis=0)))
    
    # z score with mean, std of each image
    n_all_imgs = fundus_imgs.shape[0]
    for index in range(n_all_imgs):
        mean = np.mean(fundus_imgs[index, ...][fundus_imgs[index, ..., 0] > 40.0], axis=0)
        std = np.std(fundus_imgs[index, ...][fundus_imgs[index, ..., 0] > 40.0], axis=0)
        assert len(mean) == 3 and len(std) == 3
        fundus_imgs[index, ...] = (fundus_imgs[index, ...] - mean) / std
 
    if fov_mask:
        return fundus_imgs, mask_imgs, fov
    else:
        return fundus_imgs, mask_imgs


def pixel_values_in_mask(true_vessels, pred_vessels, masks, split_by_img=False):
    assert np.max(pred_vessels) <= 1.0 and np.min(pred_vessels) >= 0.0
    assert np.max(true_vessels) == 1.0 and np.min(true_vessels) == 0.0
    assert np.max(masks) == 1.0 and np.min(masks) == 0.0
    assert pred_vessels.shape[0] == true_vessels.shape[0] and masks.shape[0] == true_vessels.shape[0]
    assert pred_vessels.shape[1] == true_vessels.shape[1] and masks.shape[1] == true_vessels.shape[1]
    assert pred_vessels.shape[2] == true_vessels.shape[2] and masks.shape[2] == true_vessels.shape[2]
    
    if split_by_img:
        n = pred_vessels.shape[0]
        return np.array([true_vessels[i, ...][masks[i, ...] == 1].flatten() for i in range(n)]), np.array([pred_vessels[i, ...][masks[i, ...] == 1].flatten() for i in range(n)]) 
    else: 
        return true_vessels[masks == 1].flatten(), pred_vessels[masks == 1].flatten() 


def remain_in_mask(imgs, masks):
    imgs[masks == 0] = 0
    return imgs


def load_images_under_dir(path_dir):
    files = all_files_under(path_dir)
    return imagefiles2arrs(files)


def crop_to_original(imgs, ori_shape):
    pred_shape = imgs.shape
    assert len(pred_shape) < 4

    if ori_shape == pred_shape:
        return imgs
    else: 
        if len(imgs.shape) > 2:
            ori_h, ori_w = ori_shape[1], ori_shape[2]
            pred_h, pred_w = pred_shape[1], pred_shape[2]
            return imgs[:, (pred_h - ori_h) // 2:(pred_h - ori_h) // 2 + ori_h, (pred_w - ori_w) // 2:(pred_w - ori_w) // 2 + ori_w]
        else:
            ori_h, ori_w = ori_shape[0], ori_shape[1]
            pred_h, pred_w = pred_shape[0], pred_shape[1]
            return imgs[(pred_h - ori_h) // 2:(pred_h - ori_h) // 2 + ori_h, (pred_w - ori_w) // 2:(pred_w - ori_w) // 2 + ori_w]


class Scheduler:

    def __init__(self, n_itrs_per_epoch_d, n_itrs_per_epoch_g, schedules, init_lr):
        self.schedules = schedules
        self.init_dsteps = n_itrs_per_epoch_d
        self.init_gsteps = n_itrs_per_epoch_g
        self.init_lr = init_lr
        self.dsteps = self.init_dsteps
        self.gsteps = self.init_gsteps
        self.lr = self.init_lr

    def get_dsteps(self):
        return self.dsteps
    
    def get_gsteps(self):
        return self.gsteps
    
    def get_lr(self):
        return self.lr
        
    def update_steps(self, n_round):
        key = str(n_round)
        if key in self.schedules['lr_decay']:
            self.lr = self.init_lr * self.schedules['lr_decay'][key]
        if key in self.schedules['step_decay']:
            self.dsteps = max(int(self.init_dsteps * self.schedules['step_decay'][key]), 1)
            self.gsteps = max(int(self.init_gsteps * self.schedules['step_decay'][key]), 1)
