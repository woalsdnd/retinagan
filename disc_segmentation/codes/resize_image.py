from PIL import Image
from scipy.misc import imresize
import utils
import numpy as np
import os 

h_target, w_target = 640, 640
source_dirs = ["../data/original/Drishti-GS1/Training/Images", "../data/original/Drishti-GS1/Training/Masks",
               "../data/original/Drishti-GS1/Test/Images", "../data/original/Drishti-GS1/Test/Masks"]
target_dirs = ["../data/preprocessed/drishti/training/images", "../data/preprocessed/drishti/training/masks",
               "../data/preprocessed/drishti/test/images", "../data/preprocessed/drishti/test/masks"]

for index in range(len(source_dirs)):
    source_dir = source_dirs[index]
    target_dir = target_dirs[index]
    utils.mkdir_if_not_exist(target_dir)
    for filepath in utils.all_files_under(source_dir):
        img = Image.open(filepath)
        resized_img = imresize(img, (h_target, w_target), 'bilinear')
        outpath = os.path.join(target_dir, os.path.basename(filepath))
        Image.fromarray(resized_img.astype(np.uint8)).save(outpath)
