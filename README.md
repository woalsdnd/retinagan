#RetinaGAN #
## Towards Accurate Segmentation of Retinal Vessels and the Optic Disc in Fundoscopic Images with Generative Adversarial Networks ##

### most codes for vessel segmentation came from from [V-GAN](https://bitbucket.org/woalsdnd/v-gan) except a comparison with one method  ###

![bitbucket_header.jpg](imgs/vessel.jpg)
![bitbucket_header_od.jpg](imgs/od.jpg)

## LICENSE ##
This is under the MIT License  
Copyright (c) 2018 Vuno Inc. (www.vuno.co)